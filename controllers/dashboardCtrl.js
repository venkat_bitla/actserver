// Require Modules
var Joi 		= require('joi');
var Boom 		= require('boom');
var Models        = require('../models/index');
var util 	      = require('../utils/index');

var AwsCtrl = {};
//var AwsData = Models.Aws;
var User = Models.User;


AwsCtrl.getUserAwsSummary = {
    handler: function(request, reply) {
      var userId = request.params.userId;
      
      //send data to publisher worker
      var data = {id:userId};
      var message = JSON.stringify(data);
      util.awsPublisher.isReady() && util.awsPublisher.publish(message);

      User.forge({id: parseInt(userId)})
      .related('awsdata')
      .fetch()
      .then(function(aws) {
        var resp = "";
        if(aws){
            resp = aws.toJSON();
        }
        reply(resp);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
    },
    auth: 'token'
};

module.exports = AwsCtrl;