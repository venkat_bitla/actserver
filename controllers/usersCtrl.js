// Require Modules
var Joi 		    = require('joi');
var	Boom 		    = require('boom');
var Jwt 		    = require('jsonwebtoken');
var config 	        = require('../config/index');
var util 	        = require('../utils/index');
var Models          = require('../models/index');
var _         	    = require('lodash');

// variables
var privateKey = config.key.privateKey;

var UsersCtrl = {};
var User = Models.User;

UsersCtrl.getUser = {
    handler: function(request, reply) {
      var userId = request.auth.credentials.id;
      User.forge({id: parseInt(userId)}).fetch().then(function(user) {
            reply(JSON.stringify(user.omit('password', 'isVerified')));
  		}).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
    },
    auth: 'token'
};

UsersCtrl.createUser = {
    validate: {
        payload: {
            name: Joi.string().required(),
            company: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().required()
        }
    },
    handler: function(request, reply) {
    	var hashPassword = util.hash(request.payload.password);
        var email = request.payload.email;
        var emailToken  = util.encrypt(email);
        User.forge({email:email})
		.fetch()
		.then(function (user) {
            if(user){
                var res = {
                    email: user.get('email'),
                    message: 'user with same email is already registered'
                };
                return reply(res); 
            }

            User.forge({name: request.payload.name, company: request.payload.company, email:email, password: hashPassword})
            .save().then(function(model) {
               
               //send data to workers
                var data = {email: model.get('email'), token: emailToken, action: 'new'};
                var message = JSON.stringify(data);
                util.emailPublisher.isReady() && util.emailPublisher.publish(message);
      
                var res = {
                    //id: model.get('id'),
                    emailToken: emailToken,
                    message: 'saved successfully'
                };
                reply(res);
            });
		
        }).catch(function (error) {
            reply(Boom.badImplementation(error));
		});
    },
    auth: false
};


UsersCtrl.login = {
	validate: {
    	payload: {
          email: Joi.string().email().required(),
          password: Joi.string().required()
        }
    },
    handler: function(request, reply) {
        var email =  request.payload.email;
        var password = request.payload.password;
        
        User.login(email, password)
        .then(function(user) {
            if(_.isUndefined(user.email)) {
                return reply(Boom.notFound('user is missing'));                
            }
            if(user.isVerified){
                var userId = user.id;
                var email = user.email;
                var userObj = {token: Jwt.sign(user, privateKey), id:userId, email:email};
                return reply(userObj);
            } else {
                return reply(Boom.unauthorized('token not verified'));
            }
        }).catch(function(error) {
            reply(Boom.badImplementation(error));
        });
    },
    auth: false
};

UsersCtrl.verifyEmail = {
    handler: function(request, reply) {
        var token = request.params.id;
        var decode = decodeURIComponent(token);
        var email = util.decrypt(decode);
        User.findByEmailAndVerify(email)
        .then(function(user) {
            reply(user);
        }).catch(function(error) {
            reply(Boom.badImplementation(error));
        });
    },
    auth: false
};

UsersCtrl.forgotPasswordReceiveToken = {
    validate: {
        payload: {
            password: Joi.string().required()
        }
    },
    handler: function(request, reply) {
        var password = request.payload.password;
        var token = request.params.token;
        var decode = decodeURIComponent(token);
        var email = util.decrypt(decode);
                
        User.forge({email: email}).fetch().then(function(user) {
            if (user === null){
                return reply(Boom.forbidden("invalid email"));
            }
            user.save({password: util.hash(password)}, { patch: true })
			.then(function(model) {
                var email = user.get('email');
                var res = {  email: email, message: 'password has changed'};
                reply(res);
			});			
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     

    },
    auth: false
};

UsersCtrl.forgotPasswordSendToken = {
    validate: {
        payload: {
            email: Joi.string().email().required()
        }
    },
    handler: function(request, reply) {
        var email = request.payload.email;
        
        User.forge({email: email}).fetch().then(function(user) {
            if (user === null){
                return reply(Boom.forbidden("invalid email"));
            }
            var email = user.get('email')
            var emailToken  = util.encrypt(email);
            
            //send data to workers
            var data = {email: email, token: emailToken, action: 'update'};
            var message = JSON.stringify(data);
            util.emailPublisher.isReady() && util.emailPublisher.publish(message);
            
            var res = {  emailToken: emailToken, message: 'password is send to registered email id'};
            reply(res);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
        
    },
    auth: false
};

UsersCtrl.changePassword = {
    validate: {
        payload: {
            password: Joi.string().required(),
            newPassword: Joi.string().required()
        }
    },
    handler: function(request, reply) {
        var password = request.payload.password;
        var newPassword = request.params.newPassword;
        var userId = request.params.id;
        var res;

       User.forge({id: parseInt(userId)}).fetch().then(function(user) {
            if (user === null){
                return reply(Boom.forbidden("invalid user"));
            }

            if(util.compare(user.get('password'))){
                user.save({password: util.hash(newPassword)}, { patch: true })
		    	.then(function(model) {
                    var email = user.get('email');
                    res = {  email: email, message: 'password has changed'};
			    });
            } else {
                res = {  email: email, message: 'password has not matched'};
            }
            reply(res);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });

    },
    auth: false

};



module.exports = UsersCtrl;