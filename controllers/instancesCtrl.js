// Require Modules
var Joi 		= require('joi');
var	Boom 		= require('boom');
var Models      = require('../models/index');

var InstancesCtrl = {};
var Instance      = Models.Instance;
var User          = Models.User;

InstancesCtrl.getUserInstances = {
    handler: function(request, reply) {
      var userId = request.params.userId;
      User.forge({id: parseInt(userId)}).related('instances')
      .fetch()
      .then(function(instances) {
        var instancesObj = {}
        if(instances){
            instancesObj = JSON.stringify(instances);
        }
        reply(instancesObj);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};


module.exports = InstancesCtrl;