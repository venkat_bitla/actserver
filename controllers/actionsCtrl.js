// Require Modules
var Joi 		= require('joi');
var	Boom 		= require('boom');
var Models      = require('../models/index');

var ActionsCtrl = {};
var Action      = Models.Action;
var User        = Models.User;

ActionsCtrl.getUserActions = {
    handler: function(request, reply) {
      var userId = request.params.userId;
      User.forge({id: parseInt(userId)}).related('actions').fetch().then(function(actions) {
        var actionsObj = {}
        if(actions){
            actionsObj = JSON.stringify(actions);
        }
        reply(actionsObj);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};

ActionsCtrl.getAction = {
    handler: function(request, reply) {
      var actionId = request.params.actionId;
      Action.forge({id: parseInt(actionId)}).fetch().then(function(action) {
        reply(JSON.stringify(action));
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};

ActionsCtrl.createAction = {
    validate: {
        payload: {
            userId: Joi.number().integer().required(),
            instanceId: Joi.number().integer().required(),
            scheduleId: Joi.number().integer().required(),
            name: Joi.string().required(),
            action: Joi.string().required()
        }
    },
    handler: function(request, reply) {
      var userId = request.payload.userId;
      Action.forge({
          user_id: userId,
          instance_id: request.payload.instanceId,
          schedule_id: request.payload.scheduleId,
          name: request.payload.name,
          type: request.payload.type,
        }).save().then(function(model) {
            var res = {
                id: model.get('id'),
                message: 'saved successfully'
            };
            reply(res);
        });
    },
    auth: 'token'
};

ActionsCtrl.updateAction = {
    validate: {
        payload: {
            instanceId: Joi.number().integer().required(),
            scheduleId: Joi.number().integer().required(),
            name: Joi.string().required(),
            type: Joi.string().required()
        }
    },
    handler: function(request, reply) {
       var actionId = request.params.actionId;
      Action.forge({id: actionId})
      .save({
          instance_id: request.payload.instanceId,
          schedule_id: request.payload.scheduleId,
          name: request.payload.name,
          type: request.payload.type,
        },{patch: true})
      .then(function() {
        var res = {
          id: actionId,
          message: 'updated successfully'
        };
        reply(res);
      });
    },
    auth: 'token'
};

ActionsCtrl.deleteAction = {
    handler: function(request, reply) {
      var actionId = request.params.actionId;
      Action.forge({id: parseInt(actionId)})
      .fetch()
      .then(function(action) {
         action.destroy()
         .then(function(){
            var res = {
              id: actionId,
              message: 'deleted successfully'
            };
            reply(res);
         }).catch(function (error) {
          reply(Boom.badImplementation(error));
        });

      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};


module.exports = ActionsCtrl;