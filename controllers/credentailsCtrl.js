// Require Modules
var Joi 		= require('joi');
var	Boom 		= require('boom');
var Models      = require('../models/index');
var util 	    = require('../utils/index');

var CredentialsCtrl = {};
var Credential  = Models.Credential;
var User        = Models.User;

CredentialsCtrl.getUserCredentials = {
    handler: function(request, reply) {
      var userId = request.params.userId;
      User.forge({id: parseInt(userId)}).related('credentials')
      .fetch()
      .then(function(credentials) {
        var credentialsObj = {}
        if(credentials){
            credentialsObj = JSON.stringify(credentials);
        }
        reply(credentialsObj);
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
    },
    auth: 'token'
};

CredentialsCtrl.getCredential = {
    handler: function(request, reply) {
      var credentialId = request.params.credentialId;
      Credential.forge({id: parseInt(credentialId)}).fetch().then(function(credential) {
        reply(JSON.stringify(credential));
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
    },
    auth: 'token'
};

CredentialsCtrl.createCredential = {
    validate: {
        payload: {
            userId: Joi.number().integer().required(),
            name: Joi.string().required(),
            type: Joi.string().required(),
            accessKey: Joi.string(),
            secretKey:Joi.string(),
            region:Joi.any().optional(),
        }
    },
    handler: function(request, reply) {
      var userId = request.payload.userId;
      Credential.forge({
          user_id: userId,
          name: request.payload.name,
          type: request.payload.type,
          accessKey: request.payload.accessKey,
          secretKey: request.payload.secretKey,
          region:request.payload.region,
        })
        .save()
        .then(function(model) {
            var res = {
                id: model.get('id'),
                message: 'saved successfully'
            };

            //send data to publisher worker
            var data = {id:userId};
            var message = JSON.stringify(data);
            util.awsInstancesPublisher.isReady() && util.awsInstancesPublisher.publish(message);

            reply(res);

        });
    },
    auth: 'token'
};

CredentialsCtrl.updateCredential = {
    validate: {
        payload: {
            name: Joi.string().optional(),
            type: Joi.string().required(),
            accessKey: Joi.any().optional(),
            secretKey:Joi.any().optional(),
            region:Joi.any().optional(),
        }
    },
    handler: function(request, reply) {
       var credentialId = request.params.credentialId;
      Credential.forge({id: credentialId})
      .save({
          name: request.payload.name,
          type: request.payload.type,
          accessKey: request.payload.accessKey,
          secretKey:request.payload.secretKey,
          region:request.payload.region,
          
        },{patch: true})
      .then(function() {
        var res = {
          id: credentialId,
          message: 'updated successfully'
        };
        reply(res);
      });
    },
    auth: 'token'
};

CredentialsCtrl.deleteCredential = {
    handler: function(request, reply) {
      var credentialId = request.params.credentialId;
      Credential.forge({id: parseInt(credentialId)}).fetch().then(function(credential) {
         credential.destroy()
         .then(function(){
            var res = {
              id: credentialId,
              message: 'deleted successfully'
            };
            reply(res);
         }).catch(function (error) {
          reply(Boom.badImplementation(error));
        });
      
      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });     
    },
    auth: 'token'
};


module.exports = CredentialsCtrl;