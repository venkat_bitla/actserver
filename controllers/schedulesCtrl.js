// Require Modules
var Joi 		    = require('joi');
var	Boom 		    = require('boom');
var Jwt 		    = require('jsonwebtoken');
var config 	        = require('../config/index');
var util 	        = require('../utils/index');
var Models          = require('../models/index');
var _         	    = require('lodash');


var SchedulesCtrl = {};
var Schedule      = Models.Schedule;
var User          = Models.User;

SchedulesCtrl.getUserSchedules = {
    handler: function(request, reply) {
        var userId = request.params.userId;
        User.forge({id: parseInt(userId)}).related('schedules').fetch().then(function(schedules) {
            reply(JSON.stringify(schedules));
        }).catch(function (error) {
            reply(Boom.badImplementation(error));
        });
    },
    auth: 'token'
};

SchedulesCtrl.getSchedule = {
    handler: function(request, reply) {
      var scheduleId = request.params.scheduleId;
      new Schedule({id: scheduleId}).fetch().then(function(schedule) {
        var cron = schedule.get('cron');
        var scheduleObj = {};

        if(cron){

          var cronSplit = cron.split(' ');
          scheduleObj.hour = cronSplit[0];
          scheduleObj.minute = cronSplit[1];

          if(cronSplit[2] != "*"){
            scheduleObj.day = cronSplit[2];
          }

          if(cronSplit[4] != "*"){
            scheduleObj.week = cronSplit[4];
          }

        }
        scheduleObj.name = schedule.get('name');
        scheduleObj.id = schedule.get('id');
        scheduleObj.cron = schedule.get('cron');
        scheduleObj.recurrence = schedule.get('recurrence');

        reply(scheduleObj);

      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};

SchedulesCtrl.createSchedule = {
    validate: {
        payload: {
            userId: Joi.number().integer().required(),
            name: Joi.string().required(),
            recurrence: Joi.string().required(),
            minute: Joi.number().integer().required(),
            hour: Joi.number().integer().required(),
            day: Joi.array(),
            week: Joi.array()
        }
    },
    handler: function(request, reply) {
      var hour = request.payload.hour;
      var minute = request.payload.minute;
      var week = request.payload.week || "*";
      var day = request.payload.day || "*";
      var cron = [ minute, hour, day, "*", week].join(' ');

      Schedule.forge({
          user_id: request.payload.userId,
          name: request.payload.name,
          recurrence: request.payload.recurrence,
          cron: cron,
        })
        .save()
        .then(function(model) {
          var res = {
            id: model.get('id'),
            message: 'saved successfully'
          };
          reply(res);

        });
    },
    auth: 'token'
};

SchedulesCtrl.updateSchedule = {
    validate: {
        payload: {
            name: Joi.string().required(),
            recurrence: Joi.string().required(),
            minute: Joi.number().integer().required(),
            hour: Joi.number().integer().required(),
            day: Joi.array(),
            week: Joi.array()
        }
    },
    handler: function(request, reply) {
       var scheduleId = request.params.scheduleId;
       var hour = request.payload.hour;
       var minute = request.payload.minute;
       var week = request.payload.week || "*";
       var day = request.payload.day || "*";
       var cron = [ minute, hour, day, "*", week].join(' ');

      Schedule.forge({id: scheduleId})
      .save({
      		name: request.payload.name,
            recurrence: request.payload.recurrence,
            cron: cron,
        },{patch: true})
      .then(function(model) {
        var res = {
          id: model.get('id'),
          message: 'saved successfully'
        };
        reply(res);
      });
    },
    auth: 'token'
};

SchedulesCtrl.deleteSchedule = {
    handler: function(request, reply) {
      var scheduleId = request.params.scheduleId;
      new Schedule({id: scheduleId}).fetch().then(function(schedule) {
         schedule.destroy()
         .then(function(model){
            var res = {
              id: scheduleId,
              message: 'delted successfully'
            };
            reply(res);
         }).catch(function (error) {
          reply(Boom.badImplementation(error));
        });

      }).catch(function (error) {
        reply(Boom.badImplementation(error));
      });
    },
    auth: 'token'
};

module.exports = SchedulesCtrl;