var util 	= require('../utils/index');

exports.seed = function(knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('users').del(), 

    // Inserts seed entries
    knex('users').insert({id: 1, name:'venkat bitla', company: 'stackinsight', email: 'venkat.bitla@gmail.com', 
      password: util.hash('password'),  isVerified: true, 
      created_at: new Date().toISOString()})
  );
};
