var API = require('./api');

exports.summary = function (user) {
        var url = 'http://api.stackinsight.com/v1/' + user.data.id + '/summary';
        return API.get(url, user.session);
}