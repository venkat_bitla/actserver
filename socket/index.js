var jwt 		= require('jsonwebtoken')
var handlers 	= require('./handler');
var config 	    = require('../config/index');

var stackSocket = {
	register: function (server, options, next) {
		var io = require('socket.io')(server.listener);

		io.on('connection', function(socket){

			delete io.sockets.connected[socket.id];

			var authenticate = function (data) {
				var options = {
      				secret: config.key.privateKey,
      				timeout: 5000 // 5 seconds to send the authentication message
			 	}

				jwt.verify(data.token, options.secret, options, function(err, decoded) {
					if (err){
          				socket.disconnect('unauthorized');
        			}

					if (!err && decoded) {

 						io.sockets.connected[socket.id] = socket;
                    	socket.tokenData = decoded;
						socket.connectedAt = new Date();
						console.info('SOCKET [%s] CONNECTED', socket.id);
                    	socket.emit('authenticated');
						socket.emit('news', { hello: 'world' });

                        var interval = setInterval(function () {
                            handlers.summary({session: data.token, data:socket.tokenData})
                            .then(function(res){
                                socket.emit("summary", res);
                            });
                        }, 60 * 1000);

                    	socket.on('disconnect', function(){
						    clearInterval(interval);
                            console.info('SOCKET [%s] DISCONNECTED', socket.id);
						});


					} else {
                    	console.log(err);
                	}
				});
			}

			socket.on('authenticate', authenticate );


  		});
			
   		next();
	}
}

stackSocket.register.attributes = {
	name: 'stackSocket',
	version: '1.0.0'
};

module.exports = stackSocket;