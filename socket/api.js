var Promise = require('bluebird');
var request = require('request');

API = {};

API.get = function (url, session ) {

	var options = {
		method: "GET",
        url: url,
        headers: {
            'Authorization': 'Bearer ' + session
        },
	    json: true
	};

	return new Promise(function (resolve, reject) {
	    request(options, function(error, response, body){
		    if(error){
				reject(error);
		    }
		    resolve(body);
	    });
	});

};


module.exports = API;