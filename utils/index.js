var config 		= require('../config/index');
var crypto 		= require('crypto');
var algorithm 	= 'aes-256-ctr';
var bcrypt      = require('bcrypt');
var Publisher 	= require('../workers/publisher');

var privateKey = config.key.privateKey;

function _encrypt(text){
  var cipher = crypto.createCipher(algorithm, privateKey);
  var crypted = cipher.update(text,'utf8','hex');
  crypted += cipher.final('hex');
  return crypted;
}
 
function _decrypt(text){
  var decipher = crypto.createDecipher(algorithm, privateKey);
  var dec = decipher.update(text,'hex','utf8');
  dec += decipher.final('utf8');
  return dec;
}
 
exports.decrypt = function(password) {
	return _decrypt(password);
};

exports.encrypt = function(password) {
	return _encrypt(password);
};

exports.hash = function (text){
    return bcrypt.hashSync(text, 10);
};

exports.compare =function (text, hash){
    return bcrypt.compareSync(text, hash);
};

exports.formatJson = function(root, json) {
    return '{"' + root + '":' + JSON.stringify(json) + '}';
};

exports.emailPublisher  = new Publisher(config.ampq.url, 'stack.email');
exports.awsPublisher  = new Publisher(config.ampq.url, 'stack.aws');
exports.awsInstancesPublisher  = new Publisher(config.ampq.url, 'stack.awsInstances');
