var AWS 	= require('aws-sdk');
var Promise = require('bluebird');

module.exports = S3Client;

function S3Client(options) {
	this.s3 = new AWS.S3(options);
	Promise.promisifyAll(Object.getPrototypeOf(this.s3));
}

S3Client.prototype.gets3Buckets = function(){
	return this.s3.listBucketsAsync()
	.then(function(data){
		return data.Buckets;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
};


S3Client.prototype.getS3Objects = function(bucketName){
	return this.s3.listObjectsAsync({Bucket:bucketName})
	.then(function (data) {
    	//var length = data.Contents.length
        console.log(data);
		if (data.IsTruncated == true){
        
		} else {
        
		}
    }).catch(function(err){
		console.log(err); // an error occurred
	});
	
};
