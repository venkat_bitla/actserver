var AWS 	= require('aws-sdk');
var Promise = require('bluebird');
var _       = require('lodash');
module.exports = EC2Client;

function EC2Client(options) {
	this.ec2 = new AWS.EC2(options);
	Promise.promisifyAll(Object.getPrototypeOf(this.ec2));
}

EC2Client.prototype.getEC2InstanceAndIpLimit = function(){
	return this.ec2.describeAccountAttributesAsync()
	.then(function(data) {
		var instances = _.pluck(_.filter(data.AccountAttributes, { 'AttributeName': 'max-instances' }), 'AttributeValues');
		var elesticIps = _.pluck(_.filter(data.AccountAttributes, { 'AttributeName': 'max-elastic-ips' }), 'AttributeValues');
		var instanceObj = _.first(_.flatten(instances));
		var elestciIpObj = _.first(_.flatten(elesticIps));
		var maxObj = {ec2Instances : instanceObj.AttributeValue, elasticIps: elestciIpObj.AttributeValue}
		return maxObj;
	}).catch(function(err){
		console.log(err); // an error occurred
	});
};

EC2Client.prototype.getInstances = function(){
	return this.ec2.describeInstancesAsync()
	.then(function(data){
		return data.Reservations;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
};

EC2Client.prototype.getElasticIPs = function () {
	return this.ec2.describeAddressesAsync()
	.then(function(data){
		return data.Addresses;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
 };
 
 EC2Client.prototype.getSecurityGroups = function () {
	return this.ec2.describeSecurityGroupsAsync()
	.then(function(data){
		return data.SecurityGroups;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
 };
 
 // EBS
EC2Client.prototype.getEBSVolumes = function () {
	return this.ec2.describeVolumesAsync()
	.then(function(data){
		return data.Volumes;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
 };
 
EC2Client.prototype.getEBSSnapshots = function () {
	return this.ec2.describeSnapshotsAsync()
	.then(function(data){
		return data.Snapshots;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
 };