var AWS 	= require('aws-sdk');
var Promise = require('bluebird');

module.exports = ELBClient;

function ELBClient(options) {
	this.elb = new AWS.ELB(options);
	Promise.promisifyAll(Object.getPrototypeOf(this.elb));
}

ELBClient.prototype.getELBs = function(){
	return this.elb.describeLoadBalancersAsync()
	.then(function(data){
		return data.LoadBalancerDescriptions;
 	}).catch(function(err){
		console.log(err); // an error occurred
	});
};
