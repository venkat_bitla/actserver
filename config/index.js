/* global process */
module.exports = {
	domain: 'app.actstack.com',
	server: {
		host: 'localhost',
		port: 8080
	},
	database: {
		client: 'postgres',
		connection: {
  			host: 'localhost',
    		user: process.env.DB_USER,
    		password: process.env.DB_PASSWORD,
    		database: 'stack',
    		port: '5432',
			charset  : 'utf8'
		},
		pool: {
    		min: 0,
    		max: 7
  		},
		migrations: {
      		tableName: 'migrations',
      		directory: './migrations'
    	},
    	seeds: {
      		directory: './seeds'
    	}   
	},
	key: {
		privateKey: 'v1sual1zey0urcl0uddataw1thstack1ns1ght',
 		tokenExpiry: 1 * 30 * 1000 * 60 //1 hour
	},
    email: {
        username: 'venkat@actstack.com',
        password: 'oxygen@19',
        accountName: 'actstack',
        verifyEmailToken: 'verifyEmail',
		forgotEmailToken: 'forgotEmail'
    },
	ampq:{
		url: process.env.RABBITMQ_URL,
		queueEmail:	"stack.email",
		queueAws: "stack.aws",
	},
	aws: {
		JOB_INTERVAL: '*/30 * * * * *',
    	LONG_JOB_INTERVAL:  '0 */30 * * * *',
    	DAILY_JOB_INTERVAL: '0 0 00 * * *',
	},
};