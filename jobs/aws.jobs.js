var CronJob 	= require('cron').CronJob;
var config 	  	= require('../config/index');
var Models      = require('../models/index');
var util 	    = require('../utils/index');
var Credential  = Models.Credential
var awsConfig 	= config.aws
var User        = Models.User;

new CronJob(awsConfig.LONG_JOB_INTERVAL, function(){
	console.log('You will see this message perodically');
    //send data to publisher worker
    User.fetchAll().then(function(users) {
        users.map(function(user){
            var data = {id:user.get('id')};
            var message = JSON.stringify(data);
            if(util.awsInstancesPublisher.isReady()){
                util.awsInstancesPublisher.publish(message);
            }
        });
    }).catch(function (error) {


    });



}, null, true, "America/Los_Angeles");
