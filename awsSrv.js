/* global accessKey */
/* global process */
var _           = require('lodash');
var Promise     = require('bluebird');
var EC2Client   = require('./utils/aws/ec2');
var ELBClient   = require('./utils/aws/elb');
var S3Client    = require('./utils/aws/s3');

var Models      = require('./models/index');
var config 	    = require('./config/index');
var Worker      = require('./workers/index');
var Credential  = Models.Credential;
var Aws         = Models.Aws;
var User        = Models.User;
var Instance    = Models.Instance;

var summary = { 
      eleastic_ips: {max:0, used: 0},
      ec2_instances:{max: 0, used: 0},
      elbs:0, 
      security_groups:0,
      s3_buckets:0,
      s3_objects:0, 
      // rdsInstances:0,
      // ecClusters:0, 
      // ecNodes:0, 
      // ecSecurityGroups:0,
      // r53HostedZones:0, 
      // r53Records:0, 
      ebs_volumes:0, 
      ebs_snapshots:0 
};

function createKeyObj(record){
      var ec2Obj = {};
      if(record.accessKey){
         ec2Obj.accessKeyId = record.accessKey;  
         ec2Obj.secretAccessKey = record.secretKey; 
         ec2Obj.region = record.region;
      }; 
      return ec2Obj
}

var getSummaryData = function(userId) {
   return User.forge({id: parseInt(userId)}).related('credentials').fetch().then(function(credentials) {
            if(credentials){
                  return createKeyObj(credentials.toJSON());
            } else {
                  throw Error("credentials not avialble");
            }
            return new Error
        }).then(function(records){
            return [new EC2Client(records), new ELBClient(records),  new S3Client(records)];
        }).spread(function(ec2, elb, s3){
            return Promise.props({
                  ipLimit:        ec2.getEC2InstanceAndIpLimit(), 
                  instances:      ec2.getInstances(),
                  elasticIps:     ec2.getElasticIPs(),
                  securityGroups: ec2.getSecurityGroups(),
                  ebsVolumes:     ec2.getEBSVolumes(),
                  ebsSnapshots:   ec2.getEBSSnapshots(),
                  elbs:           elb.getELBs(),
                  s3Buckets:      s3.gets3Buckets(),
                  //s3Objects:      s3.getS3Objects()
            });
        }).then(function(res){
              
            var securityGroups = JSON.stringify(res.securityGroups);
            var ebsVolumes     = JSON.stringify(res.ebsVolumes);
            var ebsSnapshots   = JSON.stringify(res.ebsSnapshots);
            var s3Buckets      = JSON.stringify(res.s3Buckets);
            
            summary.eleastic_ips      = {max:res.ipLimit.elasticIps, used: res.elasticIps.length};
            summary.ec2_instances     = {max:res.ipLimit.ec2Instances, used: res.instances.length};
            
            summary.security_groups   =  JSON.parse(securityGroups).length;
            summary.ebs_volumes       = JSON.parse(ebsVolumes).length;
            summary.ebs_snapshots     = JSON.parse(ebsSnapshots).length;
            summary.s3_buckets        = JSON.parse(s3Buckets).length;

            return summary;     
                   
        }).catch(function(err){
            console.log(err); // an error occurred
        });
};

function saveSummaryData(userId) {
   getSummaryData(userId).then(function(awsData){
      console.log('I am in Save');
      Aws.forge({
            user_id: userId,
            data: awsData,
      })
      .save();


   });
}

function updateSummaryData(userId) {
   getSummaryData(userId).then(function(awsData){
     console.log('I am in Update');
     User.forge({id: parseInt(userId)}).related('awsdata').fetch()
     .then(function(awsInstance) {
            var _id = awsInstance.get('id');
            Aws.forge({id: _id})
            .save({data: awsData}, {patch: true})
            .then(function(model) {
                console.log(model);
            }); 
      });     
   });
}

var awsWorker = new Worker(config.ampq.url, 'stack.aws', function (data, ack) {
      var jsonData = JSON.parse(data);
      console.log(jsonData);
      var userId = jsonData.id;

      User.forge({id: parseInt(userId)})
      .related('awsdata').fetch()
      .then(function(awsdata) {
            if (awsdata) {
                  updateSummaryData(userId);
            } else {
                  saveSummaryData(userId);
            }
            ack();
      }).catch(function (error) {
            console.log(error);
      });
 });


 var awsInstancesWorker = new Worker(config.ampq.url, 'stack.awsInstances', function (data, ack) {
       var jsonData = JSON.parse(data);
       var userId = jsonData.id;
       console.log("I am in awsInstancesWorker")
       User.forge({id: parseInt(userId)})
       .fetch({withRelated: ['credentials', 'instances']})
       .then(function(user) {
            var credentials = user.related('credentials');
            if(credentials){
                new EC2Client(createKeyObj(credentials.toJSON()))
                .getInstances()
                .then(function(awsRes){
                    _.each(awsRes, function(awsObj){
                        _.each(awsObj.Instances, function(instance){
                            console.log(instance.InstanceId)
                            Instance.forge({
                                user_id: userId,
                                instance: instance.InstanceId,
                                state: instance.State.Name,
                                type: instance.InstanceType})
                            .save().then(function() {
                                console.log('saved');
                            })
                        })
                    })
                })
            }
            ack();
       }).catch(function (error) {
             console.log(error);
       });
  });