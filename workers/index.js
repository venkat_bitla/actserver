//Worker
function Worker(rabbitServer, routingKey, workerFunction) {
	if(!rabbitServer || !routingKey || !workerFunction){
		throw new Error('Invalid args: requires rabbit server and routing key');
	}
	this.rabbitServer = rabbitServer;
	this.routingKey = routingKey;
	this.workerFunction = workerFunction;
	this._initConnection();
	
	console.log(this.routingKey)
	
}

Worker.prototype._initConnection = function () {
	var context = require('rabbit.js').createContext(this.rabbitServer);
	context.on('ready', function () {
		var worker = context.socket('WORKER', {persistent: true, prefetch: 1});
		worker.connect(this.routingKey, function () {
			worker.setEncoding('utf8');
			worker.on('data', function (message) {
				this.workerFunction(String(message), worker.ack.bind(worker));
			}.bind(this));
		}.bind(this));

	}.bind(this));
	context.on('error', function (error) {
		setTimeout(this._initConnection.bind(this), 1000);
	}.bind(this));
	
};

module.exports = Worker;