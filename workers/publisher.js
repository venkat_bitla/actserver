//Publisher
function Publisher(rabbitServer, routingKey){
	if(!rabbitServer || !routingKey) {
		throw new Error('Missing argument rabbiServer or routingKey');
	}
	this.rabbitServer = rabbitServer;
	this.routingKey = routingKey;
	this.currentPublishSocket = null;
	this._initConnection();
}

Publisher.prototype._initConnection = function(){
	var context = require('rabbit.js').createContext(this.rabbitServer);	
	context.on('ready', function(){
		var publisher = context.socket('REQ', {persistent: true});
		publisher.connect(this.routingKey, function(){
			this.currentPublishSocket = publisher;	
		}.bind(this));
	}.bind(this));	
	
	context.on('error', function(error){
			this.currentPublishSocket = null;
			setTimeout(this._initConnection.bind(this), 1000);
	}.bind(this));
	
};

Publisher.prototype.publish = function(message){
	if(!this.currentPublishSocket){
		throw new Error('Not connected to queue');
	}
	return this.currentPublishSocket.write(String(message), 'utf8');
};

Publisher.prototype.isReady = function(){
	return !!this.currentPublishSocket;
}

module.exports = Publisher;

