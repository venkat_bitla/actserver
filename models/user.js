var bookshelf 	= require('./bookshelf');
var Checkit   	= require('checkit');
var Promise  	= require('bluebird');
var Credential  = require('./credential');
var Schedule    = require('./schedule');
var AwsData  	= require('./aws');
var Action      = require('./action');
var Instance     = require('./instance');

var user = bookshelf.Model.extend({
	tableName: 'users', 
	hasTimestamps: ['created_at', 'updated_at'], 
	initialize: function(attrs, opts) {
    	this.on('saving', this.validateSave);
  	}, 
	awsdata: function() {
    	return this.hasOne(AwsData);
  	},
	credentials: function() {
    	return this.hasMany(Credential);
  	},
    schedules: function() {
       return this.hasMany(Schedule);
    },
    actions: function() {
       return this.hasMany(Action);
    },
    instances: function() {
       return this.hasMany(Instance);
    },
	validateSave: function() {
    	return new Checkit({
      		email: 'required'
    	}).run(this.attributes);
  	}

},{
	login: Promise.method(function(email, password) {
    	if (!email || !password){
			throw new Error('Email and password are both required');	
		} 
    	return new this({email: email.toLowerCase().trim()})
		.fetch()
  		.then(function(user) {
			if (!user) {
  				 //throw new Error('user not found');
				  return {email: null, message: 'user is not present'};
			}
			if(user.get('isVerified')){
				return user.save({lastLogin: new Date().toISOString()}, { patch: true })
				.then(function(model) {
            		return { email: model.get('email'), id: model.get('id'), isVerified: model.get('isVerified') };
				});
			} else {
				return {email: user.email, isVerified: user.isVerified};
			}
		});
  	}),
	
	findByEmailAndVerify: Promise.method(function(email) {
    	if (!email){
			throw new Error('Email required');	
		} 
    	return new this({email: email.toLowerCase().trim()})
		.fetch()
  		.then(function(user) {
			if (!user) {
  				throw new Error('User not found');
			 }
			return user.save({isVerified: true}, { patch: true })
			.then(function(model) {
            	return { email: model.get('email'), id: model.get('id')};
			});			 
		});
  	}),
	  
 });

module.exports = user;