var bookshelf 	= require('./bookshelf');
var User    	= require('./user');
var _         	= require('lodash');

var credential = bookshelf.Model.extend({
  tableName: 'credentials',
  user: function() {
    return this.belongsTo(User);
  },
  hasTimestamps: true,
});

module.exports = credential;