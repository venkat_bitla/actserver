var bookshelf 	= require('./bookshelf');
var User    	= require('./user');
var _         	= require('lodash');

var aws = bookshelf.Model.extend({
  tableName: 'awsdata',
  user: function() {
    return this.belongsTo(User, 'user_id');
  },
  hasTimestamps: true,
});

module.exports = aws;