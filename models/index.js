module.exports = {
  User: require('./user'),
  Credential : require('./credential'),
  Aws : require('./aws'),
  Schedule: require('./schedule'),
  Action: require('./action'),
  Instance: require('./instance')
}