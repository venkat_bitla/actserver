var bookshelf 	= require('./bookshelf');
var _         	= require('lodash');
var User    	= require('./user');
var Action      = require('./action');

module.exports =  bookshelf.Model.extend({
  tableName: 'instances',
  user: function() {
    return this.belongsTo(User, 'user_id');
  },
  action: function() {
    return this.belongsTo(Action);
  },
  hasTimestamps: true
});