var bookshelf 	= require('./bookshelf');
var _         	= require('lodash');
var User    	= require('./user');
var Instance    = require('./instance');
var Schedule    = require('./schedule');

module.exports =  bookshelf.Model.extend({
  tableName: 'actions',
  user: function() {
    return this.belongsTo(User, 'user_id');
  },
  instance: function() {
      return this.hasOne(Instance);
  },
  schedule: function() {
      return this.hasOne(Schedule);
  },
  hasTimestamps: true
});