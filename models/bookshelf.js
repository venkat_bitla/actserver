var config 		= require('../config/index');
var knex		= require('knex')(config.database);
var bookshelf 	= require('bookshelf')(knex);
module.exports 	= bookshelf;