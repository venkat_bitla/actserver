var usersCtrl			= require('./controllers/usersCtrl');
var CredentialsCtrl		= require('./controllers/credentailsCtrl');
var dashboardCtrl		= require('./controllers/dashboardCtrl');
var schedulesCtrl       = require('./controllers/schedulesCtrl');
var actionsCtrl         = require('./controllers/actionsCtrl');
var instancesCtrl         = require('./controllers/instancesCtrl');

exports.endpoints = [
	
	//User
    { method: 'GET',  path: '/v1/verifyEmail/{id}', config: usersCtrl.verifyEmail },
	{ method: 'GET',  path: '/v1/user', config: usersCtrl.getUser },
	{ method: 'POST', path: '/v1/user', config: usersCtrl.createUser },
	{ method: 'POST', path: '/v1/login', config: usersCtrl.login },
	{ method: 'POST', path: '/v1/forgotPassword/{token}', config: usersCtrl.forgotPasswordReceiveToken },
    { method: 'POST', path: '/v1/forgotPassword', config: usersCtrl.forgotPasswordSendToken },
    { method: 'POST', path: '/v1/changePassword/{id}', config: usersCtrl.changePassword },

    //Credentials
	{ method: 'GET',  	path: '/v1/{userId}/credentials', config: CredentialsCtrl.getUserCredentials },
	{ method: 'GET',  	path: '/v1/credentials/{credentialId}', config: CredentialsCtrl.getCredential },
	{ method: 'PUT',  	path: '/v1/credentials/{credentialId}', config: CredentialsCtrl.updateCredential },
	{ method: 'DELETE', path: '/v1/credentials/{credentialId}', config: CredentialsCtrl.deleteCredential },
	{ method: 'POST', 	path: '/v1/credentials', config: CredentialsCtrl.createCredential },

    //aws
	{ method: 'GET',  	path: '/v1/{userId}/summary', config: dashboardCtrl.getUserAwsSummary },

    //Schedules
	{ method: 'GET',  	path: '/v1/{userId}/schedules', config: schedulesCtrl.getUserSchedules },
	{ method: 'GET',  	path: '/v1/schedules/{scheduleId}', config: schedulesCtrl.getSchedule },
	{ method: 'PUT',  	path: '/v1/schedules/{scheduleId}', config: schedulesCtrl.updateSchedule },
	{ method: 'DELETE', path: '/v1/schedules/{scheduleId}', config: schedulesCtrl.deleteSchedule },
	{ method: 'POST', 	path: '/v1/schedules', config: schedulesCtrl.createSchedule },

    //Actions
	{ method: 'GET',  	path: '/v1/{userId}/actions', config: actionsCtrl.getUserActions },
	{ method: 'GET',  	path: '/v1/actions/{actionId}', config: actionsCtrl.getAction },
	{ method: 'PUT',  	path: '/v1/actions/{actionId}', config: actionsCtrl.updateAction },
	{ method: 'DELETE', path: '/v1/actions/{actionId}', config: actionsCtrl.deleteAction },
	{ method: 'POST', 	path: '/v1/actions', config: actionsCtrl.createAction },

    //Instances
	{ method: 'GET',  	path: '/v1/{userId}/instances', config: instancesCtrl.getUserInstances },

];