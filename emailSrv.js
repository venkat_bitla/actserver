var nodemailer 	= require("nodemailer");
var config 	    = require('./config/index');
var Worker      = require('./workers/index');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.email.username,
        pass: config.email.password
    }
});

var htmlHdrTemplate = [
     '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
     '<html xmlns="http://www.w3.org/1999/xhtml">',
     '<head>',
     '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />',
     '<title>actstack</title>',
     '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
     '</head>',
     '<body style="margin: 0; padding: 0;">'
     ].join('');

var htmlFooterTemplate = ['</body>','</html>'].join('');

var fromVal = config.email.accountName + "<" + config.email.username + ">";

var sendEmail = function (to, subject, emailContent, callback) {
    var email = {
        from: fromVal, 
        to: to, 
        subject: subject, 
        html: emailContent
    };

    transporter.sendMail(email, function(error, info) {
        callback(error, email);
    });
};

var mailRegisterBody = function(token) {
	var htmlStr = [
        '<p>Thanks for Registering on', config.email.accountName, '</p>',
        '<p>Please verify your email by clicking on the verification link below.<br/>',
        '<a href="http://', config.domain, '/#/', config.email.verifyEmailToken,
        '/', token,'">Verification Link</a></p>'].join('');

        var html = [htmlHdrTemplate, htmlStr, htmlFooterTemplate].join('');
        return html;
};

var mailForgotPasswordBody = function(token) {
	var htmlStr = [
        '<p>', config.email.accountName, '</p>',
        '<p>Please verify your email by clicking on the verification link below.<br/>',
        '<a href="http://', config.domain, '/#/', config.email.forgotEmailToken,
        '/', token,'">Verification Link</a></p>'].join('');

        var html = [htmlHdrTemplate, htmlStr, htmlFooterTemplate].join('')

        return html;
};


var worker = new Worker(config.ampq.url, 'stack.email', function (data, ack) {
  
  var jsonData = JSON.parse(data);
  var subject = "Account Verification";
  var encodeToken = encodeURIComponent(jsonData.token);
  var body = mailRegisterBody(encodeToken);
  
  if(jsonData.action == 'update'){
    body = mailForgotPasswordBody(encodeToken);
  } 
  
  setTimeout(sendEmail(jsonData.email, subject, body, function(error){
    if(error){
      console.log(error);
    } else {
      console.log("email sent");
      ack();
    }
  }), 2000);
 
 });

