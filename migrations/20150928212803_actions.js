
exports.up = function(knex, Promise) {
	return knex.schema.hasTable('actions').then(function(exists) {
		if (!exists) {
 			return knex.schema.createTable('actions', function(table) {
				table.increments('id').primary();
				table.integer('user_id').notNullable();
				table.integer('instance_id').notNullable();
				table.integer('schedule_id').notNullable();
				table.string('name').nullable();
				table.string('type').nullable();
				table.timestamps();
			});
		}  
	});
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('actions');
};
