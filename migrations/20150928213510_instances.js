
exports.up = function(knex, Promise) {
	return knex.schema.hasTable('instances').then(function(exists) {
		if (!exists) {
 			return knex.schema.createTable('instances', function(table) {
				table.increments('id').primary();
		        table.integer('user_id').notNullable();
				table.string('instance').nullable();
				table.string('state').nullable();
				table.string('type').nullable();
				table.timestamps();
			});
		}  
	});
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('actions');
};