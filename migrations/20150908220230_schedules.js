'use strict';

//name -- it will be used to create the final triggering URL (you pay use full URL here too, it does not matter)
//enabled -- 1 means it is enabled, so we check the nextRun
//cron -- the CRON like description of when to run (or in reality how to compute nextRun time)
//nextrun -- datetime of next run in UTC
//lastrun -- may be used for optimistic concurrency, not needed on Table Storage

exports.up = function(knex, Promise) {
	return knex.schema.createTable('schedules', function(table) {
		table.increments('id').primary();
		table.integer('user_id').notNullable();
		table.string('name', 100).nullable();
		table.string('recurrence', 150).nullable();
		table.string('cron', 100).nullable();
		table.timestamps();
	});  
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('schedules');  
};
