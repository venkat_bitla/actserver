
exports.up = function(knex, Promise) {
	return knex.schema.hasTable('awsdata').then(function(exists) {
		if (!exists) {
  			return knex.schema.createTable('awsdata', function(table) { 
				table.increments('id').primary();
				table.integer('user_id').notNullable();
 				table.text('data');
 				table.timestamps();
			});
		}
	});  
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('awsdata');   
};
