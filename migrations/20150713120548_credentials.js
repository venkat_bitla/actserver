
exports.up = function(knex, Promise) {
	return knex.schema.hasTable('credentials').then(function(exists) {
		if (!exists) {
 			return knex.schema.createTable('credentials', function(table) { 
				table.increments('id').primary();
				table.integer('user_id').notNullable();
				table.string('name', 150).nullable();		
				table.string('type', 100).nullable();
				table.string('accessKey', 250).nullable();
				table.string('secretKey', 250).nullable();
				table.string('region', 250).nullable();
				table.timestamps();
			});
		}
	});   
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('credentials');  
};
