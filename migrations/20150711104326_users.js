
exports.up = function(knex, Promise) {
	return knex.schema.hasTable('users').then(function(exists) {
		if (!exists) {
 			return knex.schema.createTable('users', function(table) { 
				table.increments('id').primary();
				table.string('name').nullable();
				table.string('company').nullable();
				table.string('email').nullable();
				table.string('password').nullable();
				table.boolean('isVerified').defaultTo(false);
				table.timestamp('lastLogin');		
				table.timestamps();
			});
		}  
	});
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users');  
};
