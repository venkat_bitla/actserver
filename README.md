knex
------------------------------
$ npm install knex -g

-- initialize knex
$ knex init

-- generate a migration
$ knex migrate:make users

-- command
$ knex migrate:latest

-- make seed data
$ knex seed:make contacts

-- run seed
$ knex seed:run

-- environment
$source dev.env