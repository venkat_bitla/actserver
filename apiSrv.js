var Hapi 	    = require('hapi');
var moment 	    = require('moment');
var routes 	    = require('./routes');
var config 	    = require('./config/index');
var aws_job     = require('./jobs/aws.jobs');

var privateKey = config.key.privateKey;
var ttl 	   = config.key.tokenExpiry;

var validate = function(token, request, callback) {
	var diff = moment().diff(moment(token.iat * 1000));
    if (diff > ttl) {
    	return callback(null, false);
    }
    callback(null, true, token);
};

//Server
var server 	= new Hapi.Server();

server.connection({ 
    host: config.server.host, 
    port: config.server.port,
    routes: {cors:true},
});

server.register([
    { register: require('hapi-auth-jwt2') },
    { register: require('./socket/index') },
    ], function(err) {
        
	server.auth.strategy('token', 'jwt', true, {
        key: privateKey,
    	validateFunc: validate
    });
    
    server.route(routes.endpoints);
});

server.start(function() {
	console.log("StackInsight server started @ " + server.info.uri);
});
